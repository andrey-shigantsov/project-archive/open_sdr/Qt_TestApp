#ifndef TESTLOGS_H
#define TESTLOGS_H

#include <QWidget>

namespace Ui {
class TestLogs;
}

class TestLogs : public QWidget
{
  Q_OBJECT

public:
  explicit TestLogs(QWidget *parent = nullptr);
  ~TestLogs();

  void append(QString msg);

  void clear();

private slots:
  void on_btnClear_clicked();

private:
  Ui::TestLogs *ui;
};

#endif // TESTLOGS_H
