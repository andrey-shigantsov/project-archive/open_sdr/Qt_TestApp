#ifndef TESTAPP_H
#define TESTAPP_H

#include "AbstractTestApp.h"
#include <QTimer>

namespace SDR
{

class TestControl;

class TestApp : public AbstractTestApp
{
  Q_OBJECT
public:
  TestApp(int timeout_ms, QString name);
  ~TestApp();

  void setTimeout(int ms)
  {
    AbstractTestApp::setTimeout(ms);
    timer.setInterval(ms);
  }

  inline bool isStarted(){return flagStarted;}
  inline bool isRunning(){return flagRunning;}

  void start(bool isRunning);

public slots:
  void init();
  void start();
  void pause();
  void stop();
  void step();

  virtual void prestart();
  virtual void test() = 0;
  virtual void reset();
  virtual void prestop();

private:
  QTimer timer;
  bool flagStarted, flagRunning;
};

} // SDR

#endif // TESTAPP_H
