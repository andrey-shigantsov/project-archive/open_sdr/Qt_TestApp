#include "TestApp.h"
#include "TestControl.h"

using namespace SDR;

TestApp::TestApp(int timeout_ms, QString name) :
  AbstractTestApp(timeout_ms, name)
{
  setTimeout(timeout_ms);
  flagStarted = false;
  flagRunning = false;
}

TestApp::~TestApp()
{
  if (flagStarted)
    stop();
}

void TestApp::prestart()
{

}

void TestApp::reset()
{
  ControlWidget()->Plots()->clearData();
}

void TestApp::prestop()
{

}

void TestApp::init()
{
  start(false);
}

void TestApp::start()
{
  start(true);
}

void TestApp::start(bool isRunning)
{
  if (!flagStarted)
  {
    prestart();
    flagStarted = true;
    connect(&timer, SIGNAL(timeout()), this, SLOT(test()));
  }

  flagRunning = isRunning;
  setExecState(flagRunning ? AbstractTestApp::Running : AbstractTestApp::Started);
  if (!isRunning) return;

  timer.start();
}

void TestApp::pause()
{
  timer.stop();

  flagRunning = false;
  setExecState(AbstractTestApp::Started);
}

void TestApp::stop()
{
  if (!flagStarted) return;

  pause();
  flagStarted = false;
  prestop();
  setExecState(AbstractTestApp::Stopped);

  disconnect(&timer, SIGNAL(timeout()), this, SLOT(test()));

}

void TestApp::step()
{
  if (!flagStarted)
    start(false);
  test();
}
