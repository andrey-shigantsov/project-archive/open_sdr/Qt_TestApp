#include "TestLogs.h"
#include "ui_TestLogs.h"

#include <QScrollBar>

#include <QDebug>

TestLogs::TestLogs(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TestLogs)
{
  ui->setupUi(this);
}

TestLogs::~TestLogs()
{
  delete ui;
}

void TestLogs::append(QString msg)
{
  qInfo().nospace() << windowTitle() << ": " << msg;

  bool needScroll = (ui->listWidget->verticalScrollBar()->value() == ui->listWidget->verticalScrollBar()->maximum());
  auto item = new QListWidgetItem(msg, ui->listWidget);
  if (needScroll)
    ui->listWidget->scrollToBottom();
}

void TestLogs::clear()
{
  ui->listWidget->clear();
}

void TestLogs::on_btnClear_clicked()
{
  clear();
}
