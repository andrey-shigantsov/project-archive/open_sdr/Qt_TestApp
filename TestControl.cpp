#include "TestControl.h"
#include "ui_TestControl.h"

using namespace SDR;

TestControl::TestControl(AbstractTestApp *app, int timeout_ms, QString name, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TestControl)
{
  App = app;

  ui->setupUi(this);
  ui_set_exec_timeout(timeout_ms);
  ui_set_exec_state(AbstractTestApp::Stopped);

  setName(name);

  wlogs.setWindowFlags(Qt::SubWindow);

  initShortCuts();
}

TestControl::~TestControl()
{
  destroyShortCuts();
  delete ui;
}

void TestControl::setName(QString name)
{
  setWindowTitle(name);
  wlogs.setWindowTitle(name + " Logs");
}

QList<QKeySequence> TestControl::defaultShortCutsKeys()
{
  QList<QKeySequence> keys;
  for(int i = 0; i < ACTIONS_COUNT; ++i)
  {
    switch (i)
    {
    case actStep:
      keys.append(QKeySequence(Qt::Key_F5));
      break;
    case actStart:
      keys.append(QKeySequence(Qt::Key_F6));
      break;
    case actPause:
      keys.append(QKeySequence(Qt::Key_F7));
      break;
    case actStop:
      keys.append(QKeySequence(Qt::Key_F8));
      break;
    default:
      break;
    }
  }
  return keys;
}

void TestControl::connectAction(Action act)
{
  switch (act)
  {
  case actStep:
    connect(ShortCuts[act], SIGNAL(activated()), App, SLOT(step()));
    break;
  case actStart:
    connect(ShortCuts[act], SIGNAL(activated()), App, SLOT(start()));
    break;
  case actPause:
    connect(ShortCuts[act], SIGNAL(activated()), App, SLOT(pause()));
    break;
  case actStop:
    connect(ShortCuts[act], SIGNAL(activated()), App, SLOT(stop()));
    break;
  default:
    break;
  }
}

void TestControl::disconnectAction(Action act)
{
  switch (act)
  {
  case actStep:
    disconnect(ShortCuts[act], SIGNAL(activated()), App, SLOT(step()));
    break;
  case actStart:
    disconnect(ShortCuts[act], SIGNAL(activated()), App, SLOT(start()));
    break;
  case actPause:
    disconnect(ShortCuts[act], SIGNAL(activated()), App, SLOT(pause()));
    break;
  case actStop:
    disconnect(ShortCuts[act], SIGNAL(activated()), App, SLOT(stop()));
    break;
  default:
    break;
  }
}

void TestControl::initShortCuts()
{
  QList<QKeySequence> keys = defaultShortCutsKeys();
  Q_ASSERT(keys.count() == ACTIONS_COUNT);
  for(int i = 0; i < ACTIONS_COUNT; ++i)
  {
    ShortCuts.append(new QShortcut(keys[i],this));
    connectAction((Action)i);
  }
}

void TestControl::destroyShortCuts()
{
  for (int i = 0; i < ShortCuts.count(); ++i)
  {
    disconnectAction((Action)i);
    delete ShortCuts.at(i);
  }
}

void TestControl::appendUserWidget(QWidget *Widget)
{
//  Widget->setSizePolicy(Widget->sizePolicy().horizontalPolicy(),QSizePolicy::Fixed);
  ui->UserLayout->addWidget(Widget);
}

void TestControl::deleteUserWidget(QWidget *Widget)
{
  ui->UserLayout->removeWidget(Widget);
  Widget->setParent(NULL);
  delete Widget;
}

void TestControl::appendUserStatusWidget(QWidget *Widget)
{
  ui->UserStatusLayout->addWidget(Widget);
}

void TestControl::deleteUserStatusWidget(QWidget *Widget)
{
  ui->UserStatusLayout->removeWidget(Widget);
  Widget->setParent(NULL);
  delete Widget;
}

void TestControl::setPlots(TPlotsWidget *pw)
{
  ui->Plots->deleteLater();
  ui->Plots = pw;
}

TPlotsWidget *TestControl::Plots()
{
  return ui->Plots;
}

void TestControl::ui_set_exec_timeout(int timeout_ms)
{
  ui->Timeout->setValue((double)timeout_ms/1000);
  if (App)
    App->setTimeout(timeout_ms);
}

void TestControl::ui_set_exec_controls_enabled(bool flag)
{
  ui->ExecPanel->setEnabled(flag);
}

void SDR::TestControl::on_Timeout_editingFinished()
{
  App->setTimeout(ui->Timeout->value()*1000);
}

void SDR::TestControl::on_buttonRun_clicked()
{
  App->start();
}

void SDR::TestControl::on_ButtonPause_clicked()
{
  App->pause();
}

void SDR::TestControl::on_ButtonStop_clicked()
{
  App->stop();
}

void SDR::TestControl::on_buttonSingleStep_clicked()
{
  App->step();
}


void SDR::TestControl::on_buttonReset_clicked()
{
  App->reset();
}

void TestControl::closeEvent(QCloseEvent *event)
{
  emit closed();
#if 0
  Q_UNUSED(event);
  QApplication::exit(0);
#else
  QWidget::closeEvent(event);
#endif
}

void SDR::TestControl::on_buttonHZoom_toggled(bool checked)
{
  if (checked)
  {
    Plots()->enableZoom((ui->buttonVZoom->isChecked())? Qt::Horizontal|Qt::Vertical : Qt::Horizontal);
  }
  else
  {
    if (ui->buttonVZoom->isChecked())
      Plots()->enableZoom(Qt::Vertical);
    else
      Plots()->disableZoom();
  }
}

void SDR::TestControl::on_buttonVZoom_toggled(bool checked)
{
  if (checked)
    Plots()->enableZoom((ui->buttonHZoom->isChecked())? Qt::Vertical|Qt::Horizontal : Qt::Vertical);
  else
  {
    if (ui->buttonHZoom->isChecked())
      Plots()->enableZoom(Qt::Horizontal);
    else
      Plots()->disableZoom();
  }
}

void SDR::TestControl::on_buttonHDrag_toggled(bool checked)
{
  if (checked)
    Plots()->enableDrag((ui->buttonVDrag->isChecked())? Qt::Horizontal|Qt::Vertical : Qt::Horizontal);
  else
  {
    if (ui->buttonVDrag->isChecked())
      Plots()->enableDrag(Qt::Vertical);
    else
      Plots()->disableDrag();
  }
}

void SDR::TestControl::on_buttonVDrag_toggled(bool checked)
{
  if (checked)
    Plots()->enableDrag((ui->buttonHDrag->isChecked())? Qt::Vertical|Qt::Horizontal : Qt::Vertical);
  else
  {
    if (ui->buttonHDrag->isChecked())
      Plots()->enableDrag(Qt::Horizontal);
    else
      Plots()->disableDrag();
  }
}

void TestControl::ui_set_exec_state(AbstractTestApp::ExecState state)
{
  ui->buttonRun->setEnabled(state != AbstractTestApp::Running);
  ui->ButtonPause->setEnabled(state == AbstractTestApp::Running);
  ui->ButtonStop->setEnabled(state != AbstractTestApp::Stopped);
  ui->buttonSingleStep->setEnabled(state != AbstractTestApp::Running);
  ui->buttonReset->setEnabled(state != AbstractTestApp::Stopped);
}

void TestControl::ui_set_plots_enabled(bool flag)
{
  ui->Plots->setVisible(flag);
  ui->ZoomPanel->setVisible(flag);
  ui->DragPanel->setVisible(flag);

  if (flag)
  {
    int Or = 0;
    if (ui->buttonHZoom->isChecked()) Or |= Qt::Horizontal;
    if (ui->buttonVZoom->isChecked()) Or |= Qt::Vertical;
    if (Or)
      Plots()->enableZoom((Qt::Orientation)Or);
    else
      Plots()->disableZoom();
    Or = 0;
    if (ui->buttonHDrag->isChecked()) Or |= Qt::Horizontal;
    if (ui->buttonVDrag->isChecked()) Or |= Qt::Vertical;
    if (Or)
      Plots()->enableDrag((Qt::Orientation)Or);
    else
      Plots()->disableDrag();
  }
}

bool TestControl::ui_ExecControlsEnabled()
{
  return ui->ExecPanel->isEnabled();
}

void SDR::TestControl::on_buttonLogs_clicked()
{
  wlogs.show();
}
