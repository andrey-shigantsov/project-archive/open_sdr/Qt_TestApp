#include "TestSystem.h"

#include <SDR/Qt_Addons/Controls/TControlWidgetGenerator.h>
#include <SDR/Qt_Addons/Controls/TStatusWidgetGenerator.h>
#include <SDR/Qt_Addons/Controls/TPlotsWidgetGenerator.h>
#include <SDR/TestBase/TestIO.h>

using namespace SDR;

extern "C"
{
  static HOST_IO_t * globalIO = 0;
  HOST_IO_t * HOST_IO(){return globalIO;}

  //----------------------------------------------------------------------------------

  void test_startPaused()
  {
    HOST_IO_startPaused(HOST_IO());
  }

  void test_continue()
  {
    HOST_IO_start(HOST_IO());
  }

  void test_start()
  {
    HOST_IO_start(HOST_IO());
  }

  void test_step()
  {
    HOST_IO_step(HOST_IO());
  }

  void test_pause()
  {
    HOST_IO_pause(HOST_IO());
  }

  void test_reset()
  {
    HOST_IO_reset(HOST_IO());
  }

  void test_stop()
  {
    HOST_IO_stop(HOST_IO());
  }

  Bool_t test_isStarted()
  {
    return HOST_IO_isStarted(HOST_IO());
  }

  Bool_t test_isRunning()
  {
    return HOST_IO_isRunning(HOST_IO());
  }

  void test_setExecTimeoutMs(int32_t timeout)
  {
    TARGET_Params_t params;
    params.timeout_ms = timeout;
    HOST_IO_send_params(HOST_IO(), &params);
  }

#define VA_FXN_BASE(buf,format) \
do{ \
  va_list args; \
  va_start(args,format); \
  vsnprintf(buf,sizeof(buf),format,args); \
} while(0)
  static char vaBuf[1024];
  void test_log_message(const char * format, ...)
  {
    VA_FXN_BASE(vaBuf,format);
    HOST_IO_send_log_message(HOST_IO(), vaBuf);
  }
  Bool_t test_set_control_value(TEST_DataId_t id, const char * format, ...)
  {
    VA_FXN_BASE(vaBuf,format);
    return HOST_IO_send_element_value(HOST_IO(),TARGET_ControlElement,id,vaBuf);
  }
  Bool_t test_set_control_params(TEST_DataId_t id, const char * format, ...)
  {
    VA_FXN_BASE(vaBuf,format);
    return HOST_IO_send_element_params(HOST_IO(),TARGET_ControlElement,id,vaBuf);
  }
  Bool_t test_set_status_value(TEST_DataId_t id, const char * format, ...)
  {
    VA_FXN_BASE(vaBuf,format);
    return HOST_IO_send_element_value(HOST_IO(),TARGET_StatusElement,id,vaBuf);
  }

  Bool_t test_set_plot_params(TEST_DataId_t id, const char * format, ...)
  {
    VA_FXN_BASE(vaBuf,format);
    return HOST_IO_send_plot_command_ext(HOST_IO(),id,TARGET_plotCmd_setParam, vaBuf);
  }

  Bool_t test_plot_clear(TEST_DataId_t plotId)
  {
    return HOST_IO_send_plot_command(HOST_IO(),plotId,TARGET_plotCmd_Clear);
  }

  Bool_t test_set_samples(TEST_DataId_t id, Sample_t * Buf, Size_t Count)
  {
    return HOST_IO_send_samples(HOST_IO(),id,Buf,Count);
  }
  Bool_t test_set_samples_iq(TEST_DataId_t id, iqSample_t * Buf, Size_t Count)
  {
    return HOST_IO_send_samples_iq(HOST_IO(),id,Buf,Count);
  }
}

TestSystem::TestSystem(TestData_t *test, int timeout_ms) :
  TestApp(timeout_ms, QString(testdata_name(test)))
{
  Q_ASSERT(test);
  d = test;

  HOST_IO_Cfg_t cfgIO;
  cfgIO.Master = this;
  init_HOST_IO(&io,&cfgIO);

  ucw = 0;
  usw = 0;
  setAsGlobalIO();

  char format[65536];

  testdata_sys_init(d);

  setName(testdata_name(d));

  testdata_control_format(d,format,sizeof(format));
  setControlFormat(format);

  testdata_status_format(d,format,sizeof(format));
  setStatusFormat(format);

  testdata_plots_format(d,format,sizeof(format));
  setPlotsFormat(format);

  connect(this, SIGNAL(exec_state_changed(ExecState)), this, SLOT(refresh_exec_state(ExecState)));

  if (ucw)
  {
    ucw->sendNeededValues();
  }
}

void TestSystem::setAsGlobalIO()
{
  globalIO = &io;

  if (ucw)
    ucw->sendAllValues();
}

void TestSystem::setControlFormat(const char *format)
{
  if (ucw)
  {
    disconnect(ucw,SIGNAL(valueChanged(int,QString)),this,SLOT(setControlElementValue(int,QString)));
    ControlWidget()->deleteUserWidget(ucw);
  }
  if (strlen(format))
  {
    ControlWidget()->appendUserWidget(ucw = TControlWidgetGenerator::create(format, ControlWidget()));
    connect(ucw,SIGNAL(valueChanged(int,QString)),this,SLOT(setControlElementValue(int,QString)));
  }
}

void TestSystem::setStatusFormat(const char *format)
{
  if (usw)
  {
    ControlWidget()->deleteUserStatusWidget(usw);
  }
  if (strlen(format))
    ControlWidget()->appendUserStatusWidget(usw = TStatusWidgetGenerator::create(format, ControlWidget()));
}

void TestSystem::setPlotsFormat(const char *format)
{
  ControlWidget()->Plots()->clear();
  plotsmap()->clear();
  if (strlen(format))
    if (TPlotsWidgetGenerator::read(format, ControlWidget()->Plots(), plotsmap()))
      ControlWidget()->ui_set_plots_enabled(true);
}

void TestSystem::refresh_exec_state(AbstractTestApp::ExecState state)
{
  ucw->setIsStarted(state != AbstractTestApp::Stopped);
}

void TestSystem::setControlElementValue(int id, QString val)
{
  QByteArray valBuf = val.toLocal8Bit();
  testdata_set_value(d,id,valBuf.data(),valBuf.size());
}
